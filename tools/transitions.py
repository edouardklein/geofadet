def T(root=".", static_assets='refs', gitkv_repo='localization_db'):
    from shlex import quote as q
    return [
        {
            "id": "inter",
            "error": f"{root}/errors/inter/",
            "stderr": q(root+"/logs/inter/{{.NamedMatches.id}}.log"),
            "inputs": [q(f'{root}/input/(?P<id>.*).xml')],
            "outputs": [q(root+"/inter/{{.Input 0}}")],
            "cmd": q(f"geofhandler supercheck {static_assets}/INTER.gpickle"),
            "log": q(f"{root}/logs/inter.log"),
            "side_effects": [f"{static_assets}/INTER.gpickle"]
        },
        {
            "id": "union",
            "error": f"{root}/errors/union/",
            "stderr": q(root+"/logs/union/{{.NamedMatches.id}}.log"),
            "inputs": [q(f"{root}/inter/(?P<id>.*).xml")],
            "outputs": [q(root+"/union/{{.Input 0}}")],
            "cmd": q(f"geofhandler subcheck {static_assets}/UNION.gpickle"),
            "log": q(f"{root}/logs/union.log"),
            "side_effects": [f"{static_assets}/UNION.gpickle"]
        },
        {
            "id": "convert",
            "error": f"{root}/errors/convert/",
            "stderr": q(root+"/extracted_logs/{{.NamedMatches.id}}.log"),
            "inputs": [q(f"{root}/union/(?P<id>.*).xml")],
            "outputs": [q(root+"/converted/{{.NamedMatches.id}}.csv"),
                        q(root+"/extracted_logs/{{.NamedMatches.id}}.log")],
            "cmd": q("geofhandler convert -o "
                     + f"{root}/converted/"
                     + "{{.NamedMatches.id}}.csv"),
            "log": f"{root}/logs/convert.log",
        },
        {
            "id": "extract",
            "error": f"{root}/errors/extract/",
            "stderr": q(root+"/logs/extract/{{.NamedMatches.id}}.log"),
            "inputs": [q(f"{root}/converted/(?P<id>.*).csv")],
            "outputs": [q(root+"/extracted/{{.Input 0}}"),
                        q(root+"/to_append/{{.NamedMatches.id}}.csv")],
            "cmd": '"sh -c \'tee ' + root + "/extracted/{{.Input 0}}"
            + '| geofhandler extract -o '
            + root + '/to_append/{{.NamedMatches.id}}.csv'
            + '\'"',
            "log": f"{root}/logs/extract.log",
        },
        {
            # FIXME: remove touch and make geofhandler output something
            # pmjq will redirect it to the output file, thus creating the
            # .token
            "id": "append",
            "error": f"{root}/errors/append/",
            "stderr": q(root+"/logs/append/{{.NamedMatches.id}}.log"),
            "inputs": [q(f"{root}/to_append/(?P<id>.*).csv")],
            "outputs": [q(root+"/appended/{{.NamedMatches.id}}.token")],
            "cmd": '"sh -c \'geofhandler append -r ' + gitkv_repo
            + " && touch "
            + root + '/appended/{{.NamedMatches.id}}.token\'"',
            "log": f"{root}/logs/append.log",
            "side_effects": ["GITKV"],
        },
        {
            "id": "export",
            "error": f"{root}/errors/export/",
            "stderr": q(root+"/logs/export/{{.Invariant}}.log"),
            "inputs": [q(f"{root}/extracted/(?P<id>.*).csv"),
                       q(f"{root}/appended/(?P<id>.*).token")],
            "invariant": q("$id"),
            "outputs": [q(root+"/preexported/{{.Invariant}}.csv"),
                        q(root+"/premissing/{{.Invariant}}.csv")],
            "cmd": q("geofhandler export -vvvv "
                     + f"-r {gitkv_repo} -o "
                     + root + '/preexported/{{.Invariant}}.csv'
                     + " -m " + root + '/premissing/{{.Invariant}}.csv '
                     + root + '/extracted/{{.Invariant}}.csv'),
            "log": f"{root}/logs/export.log",
            "side_effects": ["GITKV"],
        },
        {
            "id": "merge",
            "error": f"{root}/errors/merge/",
            "stderr": q(root+"/logs/merge/{{.Invariant}}.log"),
            "inputs": [q(f"{root}/preexported/(?P<id>.*).csv"),
                       q(f"{root}/premissing/(?P<id>.*).csv")],
            "invariant": q("$id"),
            "outputs": [q(root+"/exported/{{.Invariant}}.csv"),
                        q(root+"/missing/{{.Invariant}}.csv"),
                        q(root+"/merged/{{.Invariant}}.csv")],
            "cmd": '"sh -c \'cat ' + root
            + '/premissing/{{.Invariant}}.csv | tee '
            + root + '/missing/{{.Invariant}}.csv > '
            + root + '/merged/{{.Invariant}}.csv && '
            + 'cat ' + root
            + '/preexported/{{.Invariant}}.csv | '
            + 'tee ' + root
            + '/exported/{{.Invariant}}.csv | '
            + 'tail -n +2 >> '
            + root + '/merged/{{.Invariant}}.csv\'"',
            "log": f"{root}/logs/merge.log",
        },
        {
            "id": "duplicate",
            "error": f"{root}/errors/duplicate/",
            "stderr": q(root+"/logs/duplicate/{{.NamedMatches.id}}.log"),
            "inputs": [q(f"{root}/missing/(?P<id>.*).csv")],
            "outputs": [q(root+"/to_geocode/{{.NamedMatches.id}}.csv"),
                        q(root+"/to_requis/{{.NamedMatches.id}}.csv")],
            "cmd": '"sh -c \'tee ' + root
            + '/to_geocode/{{.NamedMatches.id}}.csv > '
            + root + '/to_requis/{{.NamedMatches.id}}.csv\'"',
            "log": f"{root}/logs/duplicate.log",
        },
    ]
