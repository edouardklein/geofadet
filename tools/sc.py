import sys
import os
import pmjqtools

if __name__ == "__main__":
    assert len(sys.argv) == 2
    data = sys.stdin.read()
    t = eval(data)

    os.chdir(sys.argv[1])
    pmjqtools.create_endpoints(t)
    pmjqtools.daemux_start(t)
