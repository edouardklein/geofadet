#!/bin/bash
set -e
set -u
set -x
set -o pipefail

if [ $# -lt 3 ]; then
    echo "Usage : launch.sh [--ui path/to/server domain port] repo_directory path/to/union.gpickle path/to/inter.gpickle [path/to/folders/root/]"
    exit 1
fi
launchsrc=`pwd`
if [ "$1" == "--ui" ]; then
    path="$2"
    address="$3"
    port="$4"
    shift 4
fi


if [ $# -eq 4 ]; then
    target_dir="$4"
else
    target_dir="./"
fi

# Reference files exist
if [ ! -f "$2" ] || [ ! -f "$3" ]; then
    echo "Must provide valid files !"
    exit 1
fi
echo "WARNING : This script must be run within tmux!"

# Repo directory
if [ ! -d "$1" ]; then
    mkdir -p "$1"
fi

# Target directory
if [ ! -d "$target_dir" ]; then
    mkdir -p "$target_dir"
fi

git init --bare "$1"

mkdir -p "$target_dir"/ref
cp "$2" "$target_dir"/ref/UNION.gpickle
cp "$3" "$target_dir"/ref/INTER.gpickle

srcdir=`dirname "$0"`

# Inject the given address into the cljs files
sed -e "s;<<repo>>;$1;g" $srcdir/transitions.py | python3 $srcdir/sc.py "$target_dir"


if [ ! -z "$path" ]; then
    # Cljs compilation
    resdir="$srcdir"/../pmjq_geofadet_ui/ui
    mkdir -p "$path"
    cd "$resdir"
    sed -i "s;ws://.*:[0-9]*;ws://$address:$port;" src/cljs/interf/core.cljs
    lein figwheel :once min

    # Put js files into server folder
    cd "$launchsrc"
    cp "$resdir/resources/public/index.html" "$path/index.html"
    mkdir -p "$path/js/"
    cp "$resdir/resources/public/js/app.js" "$path/js/app.js"

    # Launch websocket with pmjq_sff
    mkdir -p "$path"/dl_zone
    websocketd --staticdir="$path" --port="$port" --loglevel=debug \
        pmjq_sff --transitions="$srcdir"/transitions.py \
        --root="$target_dir" \
        --dl-folder="$path/dl_zone/" \
        --dl-url="http://$address:$port/dl_zone/"
fi
