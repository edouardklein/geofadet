# Geofadet Parser and handler

## Overview

This program is a set of functions designed to manipulate PNIJ-formatted XML files.
You can currently convert them, compare them, and check wether they correspond to a specified schema, as well as search for useful data and send it into a git repository.


## Installation

Edit the install.sh script and run it.

