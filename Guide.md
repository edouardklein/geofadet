# Geofadet
FIXME: Intégrer ce document dans la doc Shpinx
## Install

         # On the master machine
         ansible-playbook ~/Bureau/FeuillesDeRoot/ansible_playbooks/ConfigureHosts.yml
         ansible-playbook ~/Bureau/FeuillesDeRoot/ansible_playbooks/Update.yml --extra-vars "target=geofadet.lan"
         ansible-playbook ~/Bureau/FeuillesDeRoot/ansible_playbooks/ConfigureTmux.yml     --extra-vars "target=geofadet.lan"
         
         ansible-playbook tools/geofadet.yml    --extra-vars "target=geofadet.lan"
         wget https://github.com/joewalnes/websocketd/releases/download/v0.3.0/websocketd-0.3.0-linux_amd64.zip
         unzip websocket*.zip
         apt-get install openjdk-8-jre-headless openjdk-8-jdk visualvm
         update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
         wget https://raw.github.com/technomancy/leiningen/stable/bin/lein
         mv lein /usr/local/bin/lein
         echo "export LEIN_ROOT=True" >> /root/.bashrc
         chmod +x /usr/local/bin/lein
         lein --version
         git config --global user.email "Prod@geofadet.lan"


## Launch


        # First create the endpoints
        mkdir /tmp/folders; cd /tmp/folders/
        pmjq_mkendpoints --exec=~/geofadet/tools/transitions.py T
        
        # copy the .gpickle refs to the adequate folder
        mkdir /tmp/folders/refs
        cp ~/geofadet/tools/refs/*.gpickle /tmp/folders/refs/
        
        # Create the git repo
        # Don't forget to change the path inside the transition file if needed
        mkdir /tmp/repo
        git init --bare /tmp/repo
        
        # Setup the server
        mkdir -p /tmp/server/dl_zone/ /tmp/server/js
        cd ~/geofadet/pmjq_geofadet_ui/ui
        # Change the websocket address if needed 
        # (replace $address and $port by the address and port)
        # (default: localhost:8080)
        sed -i "s;ws://.*:[0-9]*;ws://$address:$port;" src/cljs/interf/core.cljs
        # Build the frontend
        lein figwheel :once min
        # Move the files to the server
        cp resources/public/index.html /tmp/server/
        cp resources/public/js/app.js /tmp/server/js/
        
        # Start the websocket (replace the por used if needed)
        websocketd --staticdir=/tmp/server --port=8080 --loglevel=debug pmjq_sff --exec=~/geofadet/tools/transitions.py --root=/tmp/folders/ --dl-folder=/tmp/server/dl_zone/ --dl-url=http://localhost:8080/dl_zone/ T
        
        # Start the pmjq processes
        # (Launch tmux first if needed)
        cd /tmp/folders
        pmjq_start --exec=~/geofadet/tools/transitions.py T


        # To stop the software from running, stop the websocket and run
        tmux kill-session -t pmjq


## Parser

### Role

The Geofadet parser's role is to manipulate .xml files to perform basic data
analysis/extraction. In other words, the parser will allow extraction of
the file's structure, and union/intersection of multiple ones. It is also
possible to create a colorized union showing the frequency of nodes within
the graph, giving an overview of the document's content.

### Typical usage

        geofparser union --color ./*.xml | dot -Tpdf > color.pdf

Create the colorized frequency graph corresponding to the union of all of
the given .xml's structure.

--------------------------------------------------------------------------------

        geofparser union ./*.xml --gpickleout -o union.gpickle
        geofparser inter ./*.xml --gpickleout -o inter.gpickle

Create the union and intersection structures and store them in easily parsed
formats (CytoscapeJS JSON format).


## Handler

### Role

The Geofadet handler's goal is to process a given .xml or .csv file through 
different verification and extraction steps, allowing its data to be 
checked and stored inside a git repository.

### Typical usage

        geofhandler subcheck union.gpickle input.xml
        geofhandler supercheck inter.gpickle input.xml

Will check that inter ⊆ to_check ⊆ union.
Used to check a file's format. union.gpickle and inter.gpickle come from geofparser.

--------------------------------------------------------------------------------

        geofhandler mt20check input.csv
        
Check that the given .csv is in MT20CSV format

--------------------------------------------------------------------------------

        geofhandler convert -o converted.csv input.xml

Convert an XML file to CSV to unify the rest of the processing chain.

--------------------------------------------------------------------------------

        geofhandler mt20convert -o converted.csv input.csv
        
Will extend the CSV by adding full date and coordinates columns.

--------------------------------------------------------------------------------

        geofhandler extract -o extracted.csv input.csv

Check the file's validity, and store the extracted data in a .csv file.

--------------------------------------------------------------------------------

        geofhandler append -r git_repo extracted.csv

Add data contained in the .csv file to the given repository.
It will be sorted and filtered.

--------------------------------------------------------------------------------

        geofhandler export -r git_repo -o exported.csv -m missing.csv input.csv

Fetch localization data from the git repository, and split the csv into 
two parts:

- exported.csv will contained localized antennas found
- missing.csv will contain the others, that must be handled manually

