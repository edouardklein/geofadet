# -*- coding: utf-8 -*-
"""
Geofconf
-----------

This module let the user configure its operating system so that Geofadet can
run (see :ref:`configuration <ref_configuration>`).

::

    Usage:
        geofconf --documentation=<doc-dir> --static-assets=<static-dir> --pmjq-root=<pmjq-root> --gitkv=<antennae-db-dir> --web-dir=<web-dir>  --port=<port> --dl-url=<base-url> --output=<conf-dir> --user=<user>

    Options:
        --documentation=<doc-dir>     Advice: /usr/share/doc/
        --static-assets=<static-dir>  Advice: /usr/share/geofadet/
        --pmjq-root=<pmjq-root>       Advice: /var/spool/geofadet
        --gitkv=<antennae-db-dir>     Advice: /var/lib/geofadet/localization_db
        --web-dir=<web-dir>           Advice: /srv/geofadet/www/
        --port=<port>                 Advice: 80
        --dl-url=<url>
        --output=<conf-dir>           Advice: /etc/geofadet/
        --user=<user>                 Advice: geofadet
"""

from docopt import docopt
import logging
import os
from pmjqtools.tools import create_endpoints
from pmjqtools.dsl import normalize, pmjq_shepherd
from jinja2 import Template

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


PMJQ_SFF_CMD = Template(
    '"pmjq_sff" "--exec={{transitions}}"'
    ' "--dl-folder={{dl_zone}}" "--dl-url={{dl_url}}"'
    ' "T(\'{{pmjq_root}}\', \'{{static_assets}}\', \'{{gitkv}}\')"')

WEBSOCKETD_CMD = Template(
    '"websocketd" "--staticdir={{web_dir}}" "--port={{port}}"'
    ' "--loglevel=debug" {{cmd}}')

SHEPHERD_HEADER = Template("""
(define-module (gnu services toto)
  #:use-module (shepherd service)
  #:use-module (shepherd support)
  #:use-module (oop goops)
  #:export ({% for id in services %}{{id}}-service {% endfor%}geofadet-service))

""")

GEOFADET_SERVICE = Template("""
(define geofadet-service
  (make <service>
    #:docstring "Main service for Geofadet"
    #:provides '(geofadet)
    #:requires '({% for id in services %}{{id}} {% endfor%})
    #:respawn? #t
    #:start
    (lambda args
      ((make-forkexec-constructor
        (list {{cmd}})
        #:log-file "{{pmjq_root}}/logs/websocketd.log")))
    #:stop
    (lambda (running . args)
      ((make-kill-destructor 2) ;SIGINT
       (slot-ref geofadet-service 'running))
	    #f)
    #:actions
    (make-actions
     (help
      "Show the help message"
      (lambda _
        (local-output "This service can start, stop and display the status of the websocketd daemon"))))))

""")

SHEPHERD_FOOTER = Template("""
(register-services {% for id in services %}{{id}}-service {% endfor %}geofadet-service)
""")


def main(args):
    """Configure geofadet"""

    # The installation step should have put the static assets
    # in args[--static-assets] and the web UI assets in
    # args[--web-dir]

    LOGGER.info("Loading the PMJQ transitions")
    ldict = locals()
    exec(open(args['--static-assets']+'/transitions.py').read(),
         globals(),
         ldict)
    transitions = [normalize(t)
                   for t in eval(
                           'T(args["--pmjq-root"], '
                           'args["--static-assets"], args["--gitkv"])',
                           globals(), ldict)]

    LOGGER.info("Creating PMJQ endpoints at "+args['--pmjq-root'])
    os.makedirs(args['--pmjq-root'], exist_ok=True)
    create_endpoints(transitions)

    LOGGER.info("Creating the GitKV database for timestampped antennae"
                " locations")
    os.makedirs(args['--gitkv'], exist_ok=True)
    os.chdir(args['--gitkv'])
    os.system('git log || git init --bare')

    LOGGER.info("Creating the DL zone and an index file in it")
    # The index file prevents websocketd from serving the DL zone's content
    # That would be a security issue
    os.makedirs(args['--web-dir']+'/dl_zone', exist_ok=True)
    with open(args['--web-dir']+'/dl_zone/index.html', 'w') as f:
        f.write('Nice Try, Marty McFly !')

    LOGGER.info("Creating the configuration file")
    os.makedirs(args['--output'], exist_ok=True)
    sff_cmd = PMJQ_SFF_CMD.render(
        transitions=args['--static-assets']+'/transitions.py',
        pmjq_root=args['--pmjq-root'],
        dl_zone=args['--web-dir']+'/dl_zone',
        dl_url=args['--dl-url'],
        static_assets=args['--static-assets'],
        gitkv=args['--gitkv'])
    websocketd_cmd = WEBSOCKETD_CMD.render(
        web_dir=args['--web-dir'],
        port=args['--port'],
        cmd=sff_cmd)
    LOGGER.info("websocketd command is:\n\t"+websocketd_cmd)
    with open(args['--output']+'/'+'geofadet.scm', 'w') as f:
        f.write(SHEPHERD_HEADER.render(services=[t['id']
                                                 for t in transitions]))
        for t in transitions:
            f.write(pmjq_shepherd(t)+'\n\n')
        f.write(GEOFADET_SERVICE.render(services=[t['id']
                                                  for t in transitions],
                                        cmd=websocketd_cmd,
                                        pmjq_root=args['--pmjq-root']))
        f.write(SHEPHERD_FOOTER.render(services=[t['id']
                                                 for t in transitions]))

    LOGGER.info("Chowning all writable resources")
    for dname in [args['--pmjq-root'], args['--gitkv'], args['--output'],
                  args['--web-dir']]:
        os.system(f"chown -R {args['--user']} {dname}")  # Python doesn't have
        # a recursive chown function :/


def entrypoint():
    arguments = docopt(__doc__)

    main(arguments)
