# -*- coding: utf-8 -*-
"""
Geofhandler
------------

Allow PNIJ-formatted XML processing.
The different functions correspond to the consecutive steps the file
will go through::

    Usage:
        geofhandler subcheck [-v]... <schema> [<input>]
        geofhandler supercheck [-v]... <schema> [<input>]
        geofhandler convert [-v]... [-o <output>] [<input>]
        geofhandler extract [-v]... [-o <output>] [<input>]
        geofhandler append [-v]... [-r <repo>] [<input>]
        geofhandler export [-v]... [--full] [-r <repo>] [-o <output>]
                           [-m <missing>] [<input>]
        geofhandler -h | --help

    Options:
        subcheck            Check that the file structure is a subgraph
                            of <schema>
        supercheck          Check that the files structure is a supergraph
                            of <schema>
        extract             Extract all useful data from the file
        append              Add data contained in a .csv to the database
        export              Export data to .csv

    ##Input
        -o <output>         The file where data will be stored
        -m <missing>        The file where incomplete data will be
                            stored
        -r <repo>           The git database repository
        <schema>            The .gpickle reference file
        <input>             The file to handle (xml until convert, csv after it)
    ##Others
        -h, --help          Show this screen.
        -v                  Verbose output [default: 0]



"""

import sys
import dateutil.parser
import dateutil.relativedelta
import csv
import itertools
import functools
import logging
import os
from docopt import docopt
from bs4 import BeautifulSoup
from geofadet.readwrite import xml2nx
from geofadet.graph_utils import is_subgraph
import networkx as nx
import gitkv
import pyproj
import tempfile


LOGGER = logging.getLogger()
DATEFORMAT = "%Y-%m-%dT%H:%M:%S.%f"

# Paths / selectors
ROOT = ["reponsetrafic"]
#   |
#   V
INDIV_PHONE = [
    "cible numerotelephone telephoneinternational",
    "cible imsi",
    "cible imei"
]
COM = ["detailstrafic detailtrafic"]
#   |
#   v
DATE = ["dateheure"]
TIMESPAN = ["duree"]
INDIV = ["correspondant"]
TYPE = ["typeappel"]
COMP = ['complementtypeappel']
OPE = ['operateuritinerance']
VOLDOWN = ['volumedonneesdescendant']
VOLUP = ['volumedonneesmontant']
#   |
#   v
PHONE = [
    "numerotelephone telephoneinternational",
    "imsi",
    "imei"
]
NUM = PHONE[:1]
IMSI = PHONE[1:2]
IMEI = PHONE[2:]
ROLE = ["role"]
LOC = ["localisation"]
CELL = ["localisation cellule"]
COORD = ["coordonnees"]
COORDX = ["x"]
COORDY = ["y"]
COORDSYS = ["typecoordonnees"]
CELLID = [
    "globalcellid cellglobalidendifier2g3g",
    "globalcellid evolvedcellglobalidentifier",
]
ADDR = ["adresse adresse2"]
COUN = ["pays"]
CITY = ["ville"]
CODE = ['codepostal']

fieldnames = [
    'Date appel',
    'Heure appel',
    'Duree appel (sec.)',
    'Type appel',
    'Complément type appel',
    'Opérateur itinérance',
    'Volume données montantes',
    'Volume données descendantes',
    'Numero correspondant',
    'IMSI correspondant',
    'Id. boitier correspondant',
    'SIM correspondant',
    'Rôle correspondant',
    'Nom correspondant',
    'Prénom correspondant',
    'Numéro abonné',
    'IMSI abonné',
    'Id. boitier abonné',
    'SIM abonné',
    'Antenne relais abonné',
    'Rôle abonné',
    'Adresse antenne relais',
    'Code postal antenne relais',
    'Ville antenne relais',
    'Alertes import',
    'Document relatif',
    'DateISO',
    'X',
    'Y',
]


# Abstraction for the MT20CSV format
mt20 = {
    'date': 'Date appel',
    'dateformat': '%d/%m/%Y',
    'time': 'Heure appel',
    'timeformat': '%H:%M:%S',
    TIMESPAN[0]: 'Duree appel (sec.)',
    TYPE[0]: 'Type appel',
    COMP[0]: 'Complément type appel',
    OPE[0]: 'Opérateur itinérance',
    VOLUP[0]: 'Volume données montantes',
    VOLDOWN[0]: 'Volume données descendantes',
    'Cornum': 'Numero correspondant',
    'Corimsi': 'IMSI correspondant',
    'Corimei': 'Id. boitier correspondant',
    # '':'SIM correspondant',
    'Corrole': 'Rôle correspondant',
    # '':'Nom correspondant',
    # '':'Prénom correspondant',
    'Abonum': 'Numéro abonné',
    'Aboimsi': 'IMSI abonné',
    'Aboimei': 'Id. boitier abonné',
    # '':'SIM abonné',
    'ant': 'Antenne relais abonné',
    'Aborole': 'Rôle abonné',
    'address': 'Adresse antenne relais',
    CODE[0]: 'Code postal antenne relais',
    CITY[0]: 'Ville antenne relais',
    # '':'Alertes import',
    # '':'Document relatif',
    COORDX[0]: 'X',
    COORDY[0]: 'Y',
    "fulldate": "DateISO",
    "addresses": ["adresse2", "adresse3", "adresse4"],
    "none": "-",  # 'empty' string
}
role_translation = {
    "terminating-party": "destination",
    "originating-to-party": "origine",
    "forwarded-to-party": "redirigé vers"
}

# ----------------------------------------------------------------------
# Utilities ------------------------------------------------------------
# ----------------------------------------------------------------------


def get_tags(tag, selectors: list):
    """Return all tags matching at least one of the given css selectors,
    starting from the given tag.

    :param tag: The source tag
    :param selectors: The selectors to check
    :return: The list of found tags
    """
    if not selectors:
        selectors = [""]
    elems = []
    for i in selectors:
        elems += tag.select(i)
    return elems


def is_leaf(tag):
    """Quick way to detect a leaf."""
    try:
        tag.childGenerator
        return False
    except AttributeError:
        return True


def get_text(tag, selectors):
    """Return all strings found in tags matching at least one of the
    given selectors, starting from the given tag, or ``[None]``

    :param tag: The source tag
    :param selectors: The list of selectors to check
    :return: The list of found strings or ``[None]``
    """
    res = []
    for s in selectors:
        for t in get_tags(tag, [s]):
            children = list(t.children)
            if "imei" in s:  # Because our carriers can't get a fucking IMEI
                # right we must only use the 14 first chars of it...
                [res.append(x[:14])
                 for x in children if is_leaf(x) and x.strip()]
            else:
                [res.append(x) for x in children if is_leaf(x) and x.strip()]
    return list(set(res)) or [None]


def get_path(name: str):
    """Return a path extracted from ``name``.

    >>> get_path("12345678901234567890")
    '12345/67/89/01/23/45/67/89/0'

    :param name: The name to transform
    :return: The corresponding path
    """
    short_error = "CellID too short ! ({})"
    name = name.strip()
    # The initial length of 5 was arbitrarily chosen
    # after sample analysis
    assert len(name) > 5, short_error.format(name)
    path = name[:5]
    path = [path] + [name[i:i+2] for i in range(5, len(name), 2)]
    return '/'.join(path)


def date_add(date: str, seconds: int):
    """Add ``val`` to a *str* date iso-formatted.

    :param date: A str containing a date
    :param seconds: The number of seconds to add
    :return: The new date (as *str*)
    """
    date = dateutil.parser.parse(date)
    date += dateutil.relativedelta.relativedelta(seconds=int(seconds))
    return date.isoformat()


def lambert_to_gps(x, y):
    """Convert Lambert II carto coordinates to WGS84."""
    source = pyproj.Proj(init='epsg:27572')
    target = pyproj.Proj(init='epsg:4326')
    x, y = pyproj.transform(source, target, x, y)
    return x, y


def same_phone(ph1, ph2):
    """ Check phone equality by handling cases where the phone number's
    header is missing.

    >>> same_phone("33678910123", "0678910123")
    True
    >>> same_phone("0678910123", "0678910123")
    True
    >>> same_phone("678910123", "44678910123")
    False
    >>> same_phone("1234", "331234")
    False


    :param ph1: Phone number to compare
    :param ph2: Phone number to compare
    :return: True if equal else False
    """
    ph1 = ph1.strip()
    ph2 = ph2.strip()
    p1 = ["33" + ph1[-9:]] if len(ph1) <= 10 else [ph1]
    p2 = ["33" + ph2[-9:]] if len(ph2) <= 10 else [ph2]
    return any((p in p2 for p in p1))


def phone_in(phone, phonelist):
    """Return true if *phone* matches any element of *phonelist*.
    Simply calls same_phone() for each element.

    :param phone: A phone identifier (phone number or IMSI usually)
    :param phonelist: A list of identifiers
    :return: True if found else False
    """
    return any((same_phone(phone, p) for p in phonelist))


# ----------------------------------------------------------------------
# Checks ---------------------------------------------------------------
# ----------------------------------------------------------------------

def check_header_id_quant(soup, val=1):
    """Assert the quantity of phone identifiers in headers is ``val``.

    :param soup: The soup to use
    :param val: The required quantity
    """
    error = "Too {} phone identifiers in header ! ({} > {})"
    num = get_text(soup, INDIV_PHONE)
    assert len(num) == val,\
        error.format("many" if len(num) > val else "few", num, val)
    return None


def check_validity(data: list, date_start: int=3, date_end: int=4,
                   comp: list=list()):
    """Check the validity of given data by checking dates.
    If dates collide on two rows, and columns at indexes ``comp``
    aren't equal, the data is considered invalid.
    Note that given rows should be sorted by starting date.

    :param data: The *dict* containing data
    :param date_start: The column index where the starting date is
    :param date_end: The column index where the ending date is
    :param comp: The list of columns (names) that must be equal
    """
    comp = comp or [1, 2]
    for d in data:  # Verified element
        for i in data:  # Checked
            # Useless cases removal (No more tests to do)
            if d[date_end] < i[date_start]:
                break
            # Useless checks removal (checked ended before verified)
            if i[date_end] < d[date_start]:
                continue
            # Data coherence check
            for k in comp:
                assert d[k] == i[k] or not (d[k] and i[k]),\
                        "Invalid dates !"
    return None


def get_valid_date(data, sdate, edate, ident="",
                   start_ind=3, end_ind=4, id_ind=0):
    """Check the validity of a date within a data set. A date is
    considered valid if it is contained by a date in the data set.
    If ``ident != ''``, the content of columns ``id_ind`` will be
    ignored if not equal to its value.

    :param data: The list of dates to use
    :param sdate: Starting date
    :param edate: Ending date
    :param ident: Optional identifier
    :param start_ind: Index of the starting date column in data elements
    :param end_ind: Index of the ending date column in data elements
    :param id_ind: Index of the identifier column in data elements
    :return: The data row if a valid one was found, False else
    """
    for d in data:
        ds, de = d[start_ind], d[end_ind]
        if de < sdate or ds > edate:
            continue
        if ident and ident != d[id_ind]:  # Wrong ID
            LOGGER.info(
                "Wrong ID: {} (caution: "
                "might imply a corrupted database!)".format(d))
            continue
        return d
    return None


@functools.lru_cache(maxsize=None)
def fetch_from_repo(repo, name):
    """Fetch CSV data from the given git repository.

    :param repo: The repository to use
    :param name: The file path and name
    :return: CSV data as a list
    """
    data = []
    if repo.os.path.exists(name):
        with repo.open(name, "r") as f:
            LOGGER.debug("Success")
            data = csv2data(f.fd)
            LOGGER.debug("Processing finished")
        LOGGER.debug("Done !")
    else:
        LOGGER.debug("Failed !")
    return data


def csv_compare_to_database(file_desc, repo, delimiter=';', full=False):
    """Fetch data from ``repo`` to complete the csv data. Return valid
    and invalid data as two lists.

    :param file_desc: The csv file to use
    :param repo: The gitkv repository
    :param delimiter: The csv column delimiter
    :param full: Add incomplete rows to the "invalid" file if True, else
        ignore them.
    :return: (valid, invalid) lists
    """
    valid = []
    invalid = []

    reader = csv.DictReader(file_desc, delimiter=delimiter)
    lane = 0
    for row in reader:
        lane += 1
        cell = row[mt20["ant"]]
        # Empty out "-" cells
        cell = None if cell == mt20["none"] else cell

        # Get full date if available, else create it
        sdate = row[mt20["fulldate"]]
        sdate = None if sdate == mt20["none"] else sdate
        if sdate is None:
            dat = row[mt20["date"]]
            tim = row[mt20["time"]]
            if dat and tim:
                LOGGER.info("Missing date, "
                            "but found a badly-formatted one!")
            else:
                LOGGER.info("Missing date!")
        ts = row[mt20[TIMESPAN[0]]]
        ts = 0 if ts == mt20["none"] else ts

        edate = ""
        if sdate:
            edate = date_add(sdate, ts)

        if None in [cell, sdate]:
            LOGGER.warning("Line {} missing information!".format(lane))
            if full:
                invalid.append([cell or "", sdate or "", edate])
            continue

        name = get_path(cell) + ".csv"

        LOGGER.debug("Trying to import data from repo...")
        data = fetch_from_repo(repo, name)
        vd = get_valid_date(data, sdate, edate, cell)
        if not (data and vd):
            invalid.append(reader_to_list(row))
        else:
            row[mt20["x"]] = vd[1]
            row[mt20["y"]] = vd[2]
            valid.append(reader_to_list(row))
    return valid, invalid

# ----------------------------------------------------------------------
# Processing and exports -----------------------------------------------
# ----------------------------------------------------------------------


def reader_to_list(rowdict, labels=fieldnames):
    """Convert a dict into a list by filtering keys by ```label``` and
    keeping the order. Mainly used to convert a csv.DictReader row into
    a list while keeping column order.

    :param rowdict: The dict to filter and convert
    :param labels: The sorted labels to fetch
    :return: A list with filtered and sorted content
    """
    li = []
    for l in labels:
        li.append(rowdict.get(l))
    return li


def data_validation(data: list, sort_index=3):
    """Attempt to create valid data by sorting and removing duplicates.

    :param data: The list containing data
    :param sort_index: The column index to sort by
    :return: A modified version of data
    """
    if not data:
        return []
    # Sort BEFORE duplicates removal
    data = sorted(data, key=lambda k: k[sort_index])
    # Duplicate removal
    data = list(k for k, _ in itertools.groupby(data))
    return data


def data2csv(file_desc, data,
             labels=('CellID', 'X', "Y", "Start", "End")):
    """Export data to the file given. Write ``labels`` as first row
    (data is a :meth:`pnijparser.handler.extract_data` item).

    :param file_desc: The file to use
    :param data: The data to insert
    :param labels: The column names to use
    """
    writer = csv.writer(file_desc)
    writer.writerow(labels)
    writer.writerows(data)
    return None


def csv2data(file_desc):
    """Import a .csv file and extract the contained data, without
    including the first row (columns description)

    :param file_desc: The file descriptor to read
    :return: A list of rows
    """
    return list(csv.reader(file_desc))[1:]


def antenna_export(data: dict, repo):
    """Read a *dict* containing data and export it to csv format in
    a folder tree corresponding to antennas' ID.
    No line duplicates should be present, and lines will be sorted.

    :param data: The dict containing data
    :param repo: The gitkv repository
    """
    for i, j in data.items():
        name = get_path(i) + ".csv"
        pat = "/".join(name.split("/")[:-1])  # Path validation
        if repo.os.path.exists(name):
            with repo.open(name, "r") as f:
                sys.stderr.write(f.name)
                j += csv2data(f)
        # Validate data, check for errors, then export it
        j = data_validation(j)
        check_validity(j)
        if not j:
            continue
        LOGGER.debug("Antenna : {}".format(j))
        repo.os.makedirs(pat, exist_ok=True)
        with repo.open(name, "w") as f:
            data2csv(f.fd, j)
    return None


def dict2csv(filedesc, fields, rows: list):
    """Write the given dict of rows into the file descriptor, with
    the given field names.
    """
    writer = csv.DictWriter(filedesc, fieldnames=fields,
                            restval=mt20["none"], delimiter=';')
    writer.writeheader()
    writer.writerows(rows)
    return None


def xml2csv(soup):
    """Extract data from an XML file soup as a MT20CSV-formatted dict.
    The soup will be checked first.

    :param soup: The soup to convert
    :return: A list of dict filled with found data
    """

    def indiv_quant(com_number, com, mini=1, maxi=3):
        """Assert the quantity of INDIV per COM in the soup is between
        mini and maxi (included).
        """
        num = len(get_tags(com, INDIV))
        assert mini <= num <= maxi, \
            "Too {} participating parties in detailTrafic number {}"\
            .format("many" if num > maxi else "few", com_number)
        return None

    def all_parties_have_numbers(com_number, com):
        """Raise an AssertionError if a party has no id."""
        com_parties = get_tags(com, INDIV)  # All participants in this com
        assert all(get_text(p, PHONE)[0] is not None for p in com_parties),\
            "One party has no number in com #{}".format(com_number)

    def only_one_target_in_com(com_number, com, target_ids):
        """Raise an AssertionError if 0 or more than 1 target id is a
        participant in the com event"""
        com_parties = get_tags(com, INDIV)  # All participants in this com
        # All phone numbers or IMSIs in this com event
        com_ids = [j for i in com_parties for j in get_text(i, PHONE) if j is not None]
        assert sum(phone_in(tgt, com_ids) for tgt in target_ids) == 1,\
            "detailTrafic number {} uses the target "\
            "({}) 0 or more than 1 time. Participating parties are {}."\
            .format(com_number, target_ids, com_ids)

    def no_duplicates(com_number, com):
        """Raise an AssertionError if a number is present twice in the
        same communication event"""
        com_parties = get_tags(com, INDIV)  # All participants in this com
        parties_ids = [set(get_text(p, PHONE)) for p in com_parties]
        all_set_couples = itertools.combinations(parties_ids, 2)
        couples_intersections = map(lambda couple: set.intersection(*couple),
                                    all_set_couples)
        for inter in couples_intersections:
            assert len(inter) == 0, "Duplicate number(s) {} in detail"\
                "Trafic #{}".format(inter, com_number)

    def only_the_target_is_localized(com_number, com, target_ids):
        """Raise an AssertionError if anybody else than the target is localized
        """
        com_parties = get_tags(com, INDIV)  # All participants in this com
        LOGGER.debug("target_is_localized, com number {}".format(com_number))
        for party in com_parties:
            party_ids = get_text(party, PHONE)
            loc_len = len(get_tags(party, LOC))
            if party_ids != [None]:
                target = any((phone_in(x, target_ids)
                              for x in party_ids if x is not None))
            else:
                target = False
            assert target or loc_len == 0, \
                "Phone number {} is localized, but should not !"\
                .format(party_ids)
            if not target:
                continue
            assert loc_len <= 1,\
                "Wrong amount of localizations in com #{}! (expected 1, got {})"\
                .format(com_number, loc_len)


    def row_from_com(com_number, com, target_ids):
        """Return the dict row from the communication event"""
        row = dict()
        for ref in mt20.keys():
            texts = get_text(com, [ref])
            if texts[0]:
                row[mt20[ref]] = texts[0]
        # Date
        datehour = get_text(com, DATE)[0]
        datehour = dateutil.parser.parse(datehour)
        row[mt20['date']] = datehour.date().strftime(
            mt20["dateformat"])
        row[mt20['time']] = datehour.time().strftime(
            mt20["timeformat"])
        row[mt20['fulldate']] = datehour.isoformat()
        timespan = get_text(com, TIMESPAN)[0]
        if timespan is None:
            logging.warning("Timespan None for com #{}, setting to 0!"
                            .format(com_number))
            row[mt20[TIMESPAN[0]]] = 0
        # Address
        addr = get_text(com, mt20["addresses"])
        if len(addr) > 0 and addr[0]:
            addr = " - ".join(addr)
        row[mt20["address"]] = addr
        # AntennID
        aid = get_text(com, CELLID)
        if aid[0]:
            row[mt20["ant"]] = aid[0]
        # IDs
        com_parties = get_tags(com, INDIV)  # All participants in this com
        for party in com_parties:
            party_ids = get_text(party, PHONE)
            if party_ids != [None]:
                target = any((phone_in(tgt, party_ids) for tgt in target_ids))
            else:
                target = False
            if target:
                prefix = "Abo"  # Target
                coord_system = get_text(party, COORDSYS)[0]
            else:
                prefix = "Cor"  # Counterparty
            role = get_text(party, ROLE)
            if len(com_parties) > 2 and role[0] == "forwarded-to-party" \
               and prefix != "Abo":
                LOGGER.warning("detailTrafic #{}, ignoring forwarded"
                               "-to party {}".format(com_number, party_ids))
                continue
            if role[0]:
                row[mt20[prefix+"role"]] = role_translation[role[0]]
            imsi = get_text(party, IMSI)
            if imsi[0]:
                row[mt20[prefix+"imsi"]] = imsi[0]
            imei = get_text(party, IMEI)
            row[mt20[prefix+"imei"]] = "imei_"+(imei[0] or "")
            row[mt20[prefix+"num"]] = get_text(party, NUM)[0]
        # GPS coordinates
        x = row.get(mt20["x"])
        y = row.get(mt20["y"])
        # Convert if needed
        if x and y and coord_system != "wgs":
            row[mt20["x"]], row[mt20["y"]] = lambert_to_gps(x, y)
        for f in fieldnames:
            if row.get(f) is None:
                row[f] = mt20["none"]
        return row

    check_header_id_quant(soup)

    rows = []
    target_phone_ids = get_text(soup, INDIV_PHONE)  # Phone numbers or IMSIs
    # For all com events
    for com_number, com in enumerate(get_tags(soup, COM)):
        try:
            indiv_quant(com_number, com)
            only_one_target_in_com(com_number, com, target_phone_ids)
            if 'data paquet' not in get_text(com, TYPE):  # Yes, paquet with a QU.
                # ENGLISH, DO YOU SPEAK IT MOTHERFUCKER ?
                # Somehow Carriers think it's OK to put an empty party
                # if the com is a data packet...
                all_parties_have_numbers(com_number, com)
            only_the_target_is_localized(com_number, com, target_phone_ids)
            no_duplicates(com_number, com)
        except AssertionError as e:
            LOGGER.error(e)
            continue
        rows.append(row_from_com(com_number, com, target_phone_ids))

    return rows


def convert(xml_desc, csv_desc):
    """Convert an XML file into a CSV formatted as MT20CSV

    :param xml_desc: The XML file descriptor
    :param csv_desc: The CSV file descriptor to write in
    """
    soup = BeautifulSoup(xml_desc.read(), "lxml")
    dict2csv(csv_desc, fieldnames, xml2csv(soup))
    return None


def csv_extract_data(file_desc, strict=False):
    """Return a list of cell data formatted as
    ``[cellid, x, y, starting_date, ending_date]``, extracted from
    the given soup.

    :param file_desc: The csv file to extract data from
    :param strict: Reject every row missing info
    :return: A list containing antenna data
    """
    reader = csv.DictReader(
        file_desc,
        fieldnames=fieldnames,
        delimiter=";")
    next(reader)
    partial_error = "Partial data : x={}, y={}, " \
                    "date={}, cellid={}, timespan={}"
    antennas = []
    # dbg = 0
    for row in reader:

        # LOGGER.debug("Row {} ====> {}".format(dbg, row))
        # dbg += 1
        cellid = row.get(mt20["ant"])
        x = row.get(mt20["x"])
        y = row.get(mt20["y"])
        date = row.get(mt20["fulldate"])
        timespan = row.get(mt20[TIMESPAN[0]])
        end_date = None
        # Treat mt20 null character as None
        res = [(i if i != mt20["none"] else None)
               for i in [cellid, x, y, date, end_date, timespan]]
        LOGGER.debug("Found data : {}".format(res))
        if res[3] and res[5]:
            res[4] = date_add(res[3], res[5])
        if None in res:  # Missing info
            LOGGER.info(
                partial_error.format(x, y, date, cellid, timespan))
            if strict:
                continue
        # Remove timespan and add data to antennas list
        antennas.append(res[:-1])
    return antennas


def csv_extract(file_desc, output_file):
    data = csv_extract_data(file_desc)
    data2csv(output_file, data)
    return


def append(file_desc, repo):
    """Append data found in the given file to the database in the given
    gitkv repository.

    :param file_desc: The file to read from
    :param repo: The gitkv repository
    """
    data = csv2data(file_desc)
    # Dict creation
    antennas = {}
    for d in data:
        if not d[0]:
            LOGGER.warning("Rejected unidentified line")
            continue
        if not antennas.get(d[0]):
            antennas[d[0]] = []
        if not (d[1] and d[2]):
            LOGGER.warning("Rejected unlocalized line")
            continue
        antennas[d[0]].append(d)
    antenna_export(antennas, repo)
    return None


def csv_export(file_desc, repo, output=None, missing=None, full=False):
    """Export data contained in ```file_desc```, by fetching geolocation
    data from ```repo```. Two files are then created, one containing the
    matching data, and the other containing incomplete data.

    :param file_desc: The csv file to use
    :param repo: The database repository
    :param output: The valid output file
    :param missing: The incomplete output file
    :param full: Add invalid data to missing if True
    """
    assert output or missing, "Not enough outputs!"
    export_file = output or sys.stdout
    missing_file = missing or sys.stdout
    LOGGER.info("Comparing to database...")
    valid, invalid = csv_compare_to_database(
        file_desc, repo=repo, full=full
    )
    LOGGER.info("Validating data...")
    invalid = data_validation(
        invalid, sort_index=fieldnames.index(mt20["fulldate"]))
    data2csv(export_file, valid, labels=fieldnames)
    data2csv(missing_file, invalid, labels=fieldnames)
    return None


def main(args):
    """Main function processing the docopt arguments."""
    # Files
    inputf = args["<input>"] or "/dev/stdin"
    schema = args["<schema>"]
    # Options
    repo = args["-r"] or ""
    verb = args["-v"] or 0
    out = args["-o"] or "/dev/stdout"
    miss = args["-m"]
    full = args["--full"]
    # Commands bindings
    # FIXME: Use args directly, remove command
    command = {
        "graph_check": args["subcheck"] + 2*args["supercheck"],
        "ext": args["extract"],
        "app": args["append"],
        "exp": args["export"],
        "conv": args["convert"],
    }

    # LOGGER.setLevel(logging.DEBUG)
    LOGGER.setLevel(0 if verb > 3 else 40 - verb * 10)
    if args["subcheck"]:
        # Check that the input xml's structure is a subgraph of <schema>
        with tempfile.TemporaryDirectory() as d:
            fpath = os.path.join(d, "tempfile")
            with open(fpath, "w") as f:
                f.write(open(inputf, "r").read())

            subg = xml2nx(fpath)
            superg = nx.read_gpickle(args["<schema>"])
            assert is_subgraph(superg, subg),\
                f"The input graph on {inputf} " \
                f"is not a subgraph of {args['<schema>']}."
            LOGGER.info("Subcheck OK.")
            with open(fpath, "r") as f, open(out, "w") as o:
                o.write(f.read())
        return 0
    elif args["supercheck"]:
        # Check that the input xml's structure is a supergraph of <schema>
        with tempfile.TemporaryDirectory() as d:
            fpath = os.path.join(d, "tempfile")
            with open(fpath, "w") as f:
                f.write(open(inputf, "r").read())

            subg = nx.read_gpickle(args['<schema>'])
            superg = xml2nx(fpath)
            assert is_subgraph(superg, subg),\
                f"The input graph on {inputf} " \
                f"is not a supergraph of {args['<schema>']}."
            LOGGER.info("Supercheck OK.")
            with open(fpath, "r") as f, open(out, "w") as o:
                o.write(f.read())
        return 0
    elif command["ext"]:
        LOGGER.info("Opening {} for extraction...".format(inputf))
        with open(inputf, "r", newline='') as i, open(out, "w") as o:
            csv_extract(i, output_file=o)

        return 0
    elif command["exp"]:
        LOGGER.info("Opening {}...".format(repo))
        with gitkv.Repo(repo) as gkvrep:
            LOGGER.info("Opening {}, {} and {} for export...".format(
                inputf, out, miss))
            with open(inputf, "r", newline='') as i, \
                    open(out, "w", newline='') as o, \
                    open(miss, "w", newline='') as m:
                csv_export(i, gkvrep, output=o, missing=m, full=full)
        return 0
    elif command["app"]:
        LOGGER.info("Opening {}...".format(repo))
        with gitkv.Repo(repo) as gkvrep:
            LOGGER.info("Opening {} to append it...".format(inputf))
            with open(inputf, "r") as i:
                append(i, gkvrep)
            gkvrep.commit_message = os.path.basename(inputf)
        return 0
    elif command["conv"]:
        LOGGER.info("Opening {} for conversion...".format(inputf))
        with open(inputf, "r") as xmlf, open(out, "w") as csvf:
            convert(xmlf, csvf)
        return 0
    LOGGER.error("Shouldn't reach here!")
    return 1


def entrypoint():
    arguments = docopt(__doc__)
    main(arguments)
