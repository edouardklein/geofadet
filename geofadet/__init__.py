import geofadet.parser
import geofadet.handler
import geofadet.readwrite
import geofadet.graph_utils
import geofadet.conf

import logging
logging.basicConfig(
    format='%(asctime) -12s %(levelname)s : %(message)s')


def geofparse():
    geofadet.parser.entrypoint()


def geofhandle():
    geofadet.handler.entrypoint()


def geofconf():
    geofadet.conf.entrypoint()


if __name__ == "__main__":
    import doctest
    doctest.testmod(geofadet.parser)
    doctest.testmod(geofadet.handler)
    doctest.testmod(geofadet.readwrite)
    doctest.testmod(geofadet.graph_utils)
    doctest.testmod(geofadet.conf)
