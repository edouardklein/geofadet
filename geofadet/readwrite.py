"""
Read/Write
-----------

This module contains functions used to read and write trees.

Those trees describe the internal structure of the XML files we get as input

Our internal format is networkx.
Our serialization format is pickle.
Our export format is DOT then PDF.

.. admonition:: Choice of serialization format

   We used to serialize everything using a custom exporter to cytoscape's JSON
   (http://js.cytoscape.org/) but it was a pain in the ass dependency-wise.

   We would have liked to keep DOT, as we use it to generate our PDFs anyway, but
   we can't as DOT serialize everything to strings, and reading the graph
   back from disk would mess with all the numerical attributes (e.g. 1 would
   become '1'...).

   We use pickle (https://networkx.github.io/documentation/stable/reference/read\
   write/gpickle.html#pickled-graphs) which is not human readable, but simple,
   robust and portable.


>>> from geofadet.graph_utils import tree_eq
>>> t1 = xml2nx('tests/test.xml')
>>> nx.readwrite.gpickle.write_gpickle(t1, 'tests/test_tmp.gpickle')
>>> t2 = nx.readwrite.gpickle.read_gpickle('tests/test_tmp.gpickle')
>>> tree_eq(t1, t2)
True
>>> tree2pdf(t1, 'tests/test_tmp.pdf')
"""
import json
import os
import networkx as nx
import logging
from bs4 import BeautifulSoup
from geofadet.graph_utils import soup2graph, graph2dot, \
    colorize_graph_frequency, analyse_frequency, name_attribution
import tempfile

LOGGER = logging.getLogger()


def tree2pdf(t, outf):
    """Write a tree into a PDF file.

    :param t: The tree to export
    :param outf: The output filename
    """
    with tempfile.TemporaryDirectory() as d:
        nx.drawing.nx_agraph.write_dot(t, d+'/toto.dot')
        return dot2pdf(d+'/toto.dot', outf)


def dot2pdf(ifile, ofile):
    """Convert a .dot file to .pdf.

    :param ifile: Input file name
    :param ofile: Output file name
    """
    return os.popen("dot -Tpdf " + ifile + " > " + ofile).close()


def xml2nx(fname, tree=None):
    """Read a XML file and return its structure as a tree

    If an existing tree is given, the one loaded from the XML file
    will be added to it.

    :return: A new tree, or copy of the given one filled with new nodes

    >>> from geofadet.graph_utils import tree_eq
    >>> t1 = nx.read_gpickle("./tests/test.gpickle")
    >>> t2 = xml2nx("./tests/test.xml")
    >>> tree_eq(t1, t2)
    True
    """
    tree = tree.copy() if tree else nx.DiGraph()
    soup = BeautifulSoup(open(fname), "lxml")
    tree = soup2graph(tree, soup.find())
    tree.graph["nbfiles"] = tree.graph.get("nbfiles", 0) + 1
    return tree


def multiple_xml2nx(filelist,
                    inter=False,
                    tree=None,
                    gpickle=False, color=False):
    """Open a list of files and merge their structure into a single tree

    If **tree** is None, then a new one will be created; otherwise, the
    given one will be used as an initial state.

    If **inter**, then only the nodes common to all files will
    be kept.

    If **gpickle**, the input files are considered as .gpickle and will be
    analysed as such.

    If **color**, the node frequency among the file will decide the node's color

    :return: A graph of the merged (union union intersection) structure of the
        input files
    """
    tree = tree.copy() if tree else nx.DiGraph()
    if "nbfiles" not in tree.graph:
        tree.graph['nbfiles'] = 1
    graph_import = nx.read_gpickle if gpickle else xml2nx  # input format
    LOGGER.debug(f"Opening {len(filelist)} file(s)")
    for counter, fname in enumerate(filelist):
        LOGGER.info("Processing file {counter} of {len(filelist)}")
        if not inter or not tree.nodes:  # Empty tree or Union
            tree = nx.compose(tree, graph_import(fname))
        else:  # inter and non empty tree
            tree = tree.subgraph(graph_import(fname))
        tree.graph["nbfiles"] += 1
        LOGGER.info("File processed.")
    if color:
        tree = colorize_graph_frequency(analyse_frequency(tree),)  # FIXME: pas clair
    return name_attribution(tree)  # FIXME: Pas clair


