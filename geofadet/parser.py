# -*- coding: utf-8 -*-
"""
Geofparser
-----------

This module allows PNIJ-formatted XML (geofadet) manipulation,
and export into .dot and .gpickle format. ::

    Usage:
        geofparser union [-v]... [--color] [--gpicklein] [--gpickleout]
            [(-o <output>)] [<input>...]
        geofparser inter [-v]... [--gpicklein] [--gpickleout]
            [(-o <output>)] [<input>...]
        geofparser compare [-v]... [--gpickleout] [--gpicklein]
            [(-o <output>)] [(-f <A>)]... [(-t <B>)]...
        geofparser included [-v]... [--gpickleA][--gpickleB]
            [-f <A>]... [-t <B>]...
        geofparser -h | --help

    Options:
        union               Create the union of the graphs
            extracted from input (if more than one)
        inter               Create the intersection of the graphs
            extracted from input (if more than one)
        compare             Compare A to B
        included            Check A $\subseteq$ B

    ##Comparison
        -f <A>              The list of files to compare
        -t <B>              The list of files to be compared to
    ##Input
        --gpicklein            Use .gpickle files instead of XML
    ##Output
        -o <output>         The output file to save in.
        --gpickleout           Save in .gpickle format instead of .dot.
        --color             Colorize nodes depending on frequency
    ##Others
        -h, --help          Show this screen.
        -v                  Verbose output [default: 0]
"""

from docopt import docopt
import networkx as nx
import logging
from geofadet.readwrite import multiple_xml2nx
from geofadet.graph_utils import is_subgraph, color_fusion, graph2dot

LOGGER = logging.getLogger()


# Main
def main(args):
    """Main function processing the docopt arguments."""
    # Files
    stdi = "/dev/stdin"
    stdo = "/dev/stdout"

    output = args.get("-o") or stdo
    inputf = args.get("<input>") or [stdi]
    fromf = args.get("-f") or [stdi]
    targetf = args.get("-t") or [stdi]

    # Options
    verb = args.get("-v")
    gpickle = args.get("--gpicklein")
    gpickleout = args.get("--gpickleout")
    gpicklea = args.get("--gpickleA")
    gpickleb = args.get("--gpickleB")
    color = args.get("--color")

    # Commands
    commands = {
        "inter": args.get("inter"),
        "compare": args.get("compare"),
        "included": args.get("included")
    }

    graphs = [None, None, nx.DiGraph()]

    LOGGER.setLevel(0 if verb > 3 else 40 - verb * 10)

    if commands["included"]:
        graphs[0] = multiple_xml2nx(filelist=fromf, gpickle=gpicklea)
        graphs[1] = multiple_xml2nx(filelist=targetf, gpickle=gpickleb)

        subgraph = is_subgraph(graphs[1], graphs[0])

        assert subgraph, "The graph doesn't follow the given schema !"
        LOGGER.info("OK.")
        return 0
    elif not commands["compare"]:  # Intersection or union
        graphs[2] = multiple_xml2nx(
            filelist=inputf, inter=commands["inter"],
            gpickle=gpickle, color=color
        )
    else:
        if inputf == targetf:
            LOGGER.error("At least one file list (source or target)"
                         " must be provided !")
        LOGGER.info("Opening source files...")
        graphs[0] = multiple_xml2nx(filelist=fromf, gpickle=gpickle)
        LOGGER.info("Opening target files...")
        graphs[1] = multiple_xml2nx(filelist=targetf, gpickle=gpickle)
        LOGGER.info("Comparing...")
        graphs[2] = color_fusion(graphs[0], graphs[1])

    # Output processing
    if gpickleout:
        nx.write_gpickle(graphs[2], output)
    else:
        with open(output, "w") as out:
            out.write(graph2dot(graphs[2], edge_quantity=color))
    return None


def entrypoint():
    arguments = docopt(__doc__)

    main(arguments)
