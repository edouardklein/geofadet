"""
Graph utils
------------

This module contains functions used to manipulate graph data."""

import networkx as nx
import math


def node_id(tag):
    """Return the node path to the given soup tag."""
    return '/'.join(
        reversed([tag.name.lower()]
                 + [p.name.lower() for p in tag.find_parents()
                    if p is not None]))


def node_name(node_path):  # For maintainability purposes
    """Return the last element of node_id.

    >>> from bs4 import BeautifulSoup
    >>> node_name(
    ...     node_id(BeautifulSoup(
    ...     open("tests/test.xml"), "lxml"
    ...     ).find("feuille1m")))
    'feuille1m'
    """
    return node_path.split('/')[-1]


def name_attribution(graph, key_name="name"):
    """
    Add ``{key_name: node_name(node)}`` to all nodes' dict...
    """
    graph = graph.copy()
    for nod in graph.nodes():
        graph.node[nod][key_name] = node_name(nod)
    return graph


# Analysis
def soup2graph(graph=None, tag=None):
    """Return the soup parse tree as a nx graph.

    Recursively fill a networkX graph, with the given BeautifulSoup tag
    being used as root.

    Nodes are identified with their path from root, meaning no loop
    should be present.

    Giving a non-empty graph will add new nodes.
    """
    graph = graph.copy() if graph else nx.DiGraph()
    graph.graph["total"] = 0
    return soup2graph_recur(graph, tag)


def soup2graph_recur(graph, tag):
    for i in tag.children:
        if not i.name:  # Empty nodes
            continue
        node = node_id(i)
        graph.add_edge(node_id(tag), node)
        data = graph.node[node]

        data["quantity"] = data.get("quantity", 0) + 1

        graph.graph["total"] += 1

        if list(i.children):
            graph = soup2graph_recur(graph, i)
    return graph


def graph2dot(graph, edge_quantity=False):
    """Read a networkX graph and return it in a string, in dot format.

    The nodes' hash is used as ID, the name as label, meaning some
    labels might
    appear more than once in the resulting file.
    """
    string = "digraph donnees {\nnode[style=filled];\n"
    for i in graph.nodes():
        for j in graph.successors(i):
            idic = graph.node[i]
            jdic = graph.node[j]

            # Check for color
            icolor = idic.get('color')
            jcolor = jdic.get('color')

            icolor = '[color="{}"]'.format(icolor if icolor else '')
            jcolor = '[color="{}"]'.format(jcolor if jcolor else '')

            string = '{}{{"{}" [label="{}"] {} }} ->' \
                     ' {{"{}" [label="{}"] {} }}'.format(
                        string,
                        i, node_name(i), icolor,
                        j, node_name(j), jcolor)
            if edge_quantity:
                q = (jdic.get('quantity')
                     / graph.graph.get("files_nb", 1))
                string = '{} [label=" {:.2f}'.format(string, q)
                string = string.rstrip(".0") + '"]'

            string += ';\n'
    string += '\n}'
    return string


def graph_diff(graph1, graph2):
    """Return the difference of graph1 and graph2, as a graph."""
    result = graph1.copy()
    result.remove_nodes_from(graph2)
    return result


def tree_eq(t1, t2):
    """Return true if t1 == t2, check the attributes as well.

    >>> t1 = nx.read_gpickle("./tests/test.gpickle")
    >>> t2 = nx.read_gpickle("./tests/test2.gpickle")
    >>> tree_eq(t1, t2)
    False
    >>> tree_eq(t1, t1.copy())
    True

    .. admonition:: Graph equality

        As we internally represent our trees as directed graphs, we use networkx's
        isomorphism checking abilities
        https://networkx.github.io/documentation/stable/reference/algorithms/isomorphism.vf2.html#digraph-matcher.

    """
    import operator
    DiGM = nx.algorithms.isomorphism.DiGraphMatcher(t1, t2,
                                                   node_match=operator.eq,
                                                   edge_match=operator.eq)
    return DiGM.is_isomorphic()


def color_separated(origin, target, decolor=True):
    """Compare and return a colorized version of origin and target.

    Nodes will be red if missing from the target, and green if present
    in target but missing from the origin.

    :param decolor: if True, reset node color beforehand
    :return: A copy of origin and target, colorized
    """
    origin = origin.copy()
    target = target.copy()
    for i in origin.node:
        if i not in target:
            origin.node[i]["color"] = "red"
        elif decolor:
            origin.node[i]["color"] = None
    for i in target.node:
        if i not in origin:
            target.node[i]["color"] = "green"
        elif decolor:
            target.node[i]["color"] = None
    return origin, target


def color_fusion(origin, target, decolor=False):
    """Merge two graphs into a colorized one.

    Nodes will be red if missing from the target, and green if present
    in target but missing from the origin.

    :param decolor: if True, reset node color beforehand
    :return: The merged, colorized graph
    """
    result = nx.compose(origin, target)
    for node in result.nodes():
        if not target.has_node(node):
            result.node[node]["color"] = "red"
        elif not origin.has_node(node):
            result.node[node]["color"] = "green"
        elif decolor:
            target.node[node]["color"] = None
    return result


def is_subgraph(supergraph, subgraph):
    """Check wether ``(subgraph == subgraph $\cap$ supergraph)``
    or not.
    If true, then subgraph is a subgraph of supergraph.

    :return: True if subgraph $\subset$ supergraph else False
    """
    return tree_eq(subgraph, subgraph.subgraph(supergraph))


def analyse_frequency(graph):
    """Analyze the frequency of each node
    The frequency is only valid IF the parent is present, and doesn't
    take other cases into account.

    :param graph: The graph to analyze
    :return: A copy of the graph with added frequencies
    """
    graph = graph.copy()
    for i, j in graph.node.items():
        q = j.get("quantity", 1)

        pred = list(graph.predecessors(i))
        assert len(pred) < 2
        parent = pred[0] if len(pred) > 0 else None
        pdata = graph.node[parent] if parent else {}
        pquant = pdata.get("quantity", 1)
        # Make sure frequency <= 1
        j["frequency"] = min(1, (q / pquant))
    return graph


def colorize_graph_frequency(graph):
    """Colorize the graph corresponding to frequencies in nodes' data
    (see analyse_frequency)
    Nodes are blue if they have a frequency of 1
    Else, they go from green (frequent) to red (rare)

    :param graph: The graph to colorize
    :return: A copy of graph with added colors depending on frequency
    """
    result = graph.copy()
    for j in result.node.values():
        if j.get("frequency"):
            col = math.ceil((1-j["frequency"])*255)
            r = str(hex(col).split("x")[-1]).zfill(2)
            v = (str(hex(255 - col).split("x")[-1]).zfill(2)
                 if col != 0
                 else "88")
            b = ("33" if col != 0 else "ff")
            j["color"] = ("#{}{}{}".format(r, v, b))
    return result
