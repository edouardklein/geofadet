#!/bin/bash
# convert used to crash when dealing with a <detailTrafic> where a number
# appears twice
# This test checks that this is not the case anymore
set -e
set -u
set -x
set -o pipefail

DIR=/tmp/geofadet-test

rm -rf ${DIR}
mkdir -p ${DIR}/union
mkdir -p ${DIR}/converted

cp tests/double.xml ${DIR}/union/
<${DIR}/union/double.xml geofhandler convert >${DIR}/converted/double.csv 2>${DIR}/convert.log

# Check an error is reported
grep 'ERROR : Duplicate number' ${DIR}/convert.log
