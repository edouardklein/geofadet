#!/bin/bash
# Whitespaces around the phone number could make the
# "only_one_target_in_com" test crash in situations similar to the
# missing_number.xml case
# This check makes sure that it is not the case anymore by expecting the
# same result as missing_number.sh

set -e
set -u
set -x
set -o pipefail

DIR=/tmp/geofadet-test

rm -rf ${DIR}
mkdir -p ${DIR}/union
mkdir -p ${DIR}/converted

cp tests/empty_corr_whitespaces.xml ${DIR}/union/
<${DIR}/union/empty_corr_whitespaces.xml geofhandler convert >${DIR}/converted/empty_corr_whitespaces.csv 2>${DIR}/convert.log

grep 'ERROR : One party has no number in com #' ${DIR}/convert.log
