#!/bin/bash
# convert used to crash when dealing with a <detailTrafic> where a number
# appears twice
# This test checks that this is not the case anymore
set -e
set -u
set -x
set -o pipefail

DIR=/tmp/geofadet-test

rm -rf ${DIR}
mkdir -p ${DIR}/union
mkdir -p ${DIR}/converted

cp tests/too_many_cellid.xml ${DIR}/union/
<${DIR}/union/too_many_cellid.xml geofhandler convert\
 >${DIR}/converted/too_many_cellid.csv 2>${DIR}/too_many_cellid.log


# Check an error is reported
grep 'ERROR : Wrong amount of localizations in com' ${DIR}/too_many_cellid.log
