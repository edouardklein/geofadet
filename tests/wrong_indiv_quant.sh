#!/bin/bash
# convert used to crash when dealing with a <detailTrafic> where a number
# appears twice
# This test checks that this is not the case anymore
set -e
set -u
set -x
set -o pipefail

DIR=/tmp/geofadet-test

rm -rf ${DIR}
mkdir -p ${DIR}/union
mkdir -p ${DIR}/converted

cp tests/too_many_indivs.xml ${DIR}/union/
cp tests/too_few_indivs.xml ${DIR}/union/
<${DIR}/union/too_many_indivs.xml geofhandler convert\
 >${DIR}/converted/too_many_indivs.csv 2>${DIR}/too_many_indivs.log
<${DIR}/union/too_few_indivs.xml geofhandler convert\
 >${DIR}/converted/too_few_indivs.csv 2>${DIR}/too_few_indivs.log


# Check an error is reported
grep 'ERROR : Too many participating parties in detailTrafic number' ${DIR}/too_many_indivs.log
grep 'ERROR : Too few participating parties in detailTrafic number' ${DIR}/too_few_indivs.log
