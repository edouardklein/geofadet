#!/bin/bash
# convert used to crash when dealing with a <detailTrafic> where one party
# does not have a number.
# This test checks that this is not the case anymore, we just reject the detailTrafic
set -e
set -u
set -x
set -o pipefail

DIR=/tmp/geofadet-test

rm -rf ${DIR}
mkdir -p ${DIR}/union
mkdir -p ${DIR}/converted

cp tests/missing_number.xml ${DIR}/union/
<${DIR}/union/missing_number.xml geofhandler convert >${DIR}/converted/missing_number.csv 2>${DIR}/convert.log

grep 'ERROR : One party has no number in com #' ${DIR}/convert.log
