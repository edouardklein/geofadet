#!/bin/bash
# convert used to crash when dealing with a <detailTrafic> where the target
# number was not in the parties
# This test checks that this is not the case anymore, we just reject the detailTrafic
set -e
set -u
set -x
set -o pipefail

DIR=/tmp/geofadet-test

rm -rf ${DIR}
mkdir -p ${DIR}/union
mkdir -p ${DIR}/converted

cp tests/missing_target.xml ${DIR}/union/
<${DIR}/union/missing_target.xml geofhandler convert >${DIR}/converted/missing_target.csv 2>${DIR}/convert.log

grep -E 'ERROR : detailTrafic number [0-9]+ uses the target' ${DIR}/convert.log
