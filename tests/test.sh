#!/bin/bash
set -e
set -u
set -x
set -o pipefail

DIR=/tmp/geofadet-test

rm -rf ${DIR}
mkdir -p ${DIR}/input
mkdir -p ${DIR}/inter
mkdir -p ${DIR}/union
mkdir -p ${DIR}/converted
mkdir -p ${DIR}/extracted
mkdir -p ${DIR}/to_append
mkdir -p ${DIR}/appended
mkdir -p ${DIR}/exported
mkdir -p ${DIR}/missing
mkdir -p ${DIR}/to_requis
mkdir -p ${DIR}/to_geocode
mkdir -p ${DIR}/db
mkdir -p ${DIR}/ref

cp tools/refs/INTER.gpickle ${DIR}/ref/
cp tools/refs/UNION.gpickle ${DIR}/ref/

git init --bare ${DIR}/db

cp tests/min.xml ${DIR}/input/
<${DIR}/input/min.xml     tee ${DIR}/inter/min.xml     | geofhandler supercheck ${DIR}/ref/INTER.gpickle
<${DIR}/inter/min.xml     tee ${DIR}/union/min.xml     | geofhandler subcheck ${DIR}/ref/UNION.gpickle
<${DIR}/union/min.xml                                    geofhandler convert                   > ${DIR}/converted/min.csv
<${DIR}/converted/min.csv tee ${DIR}/extracted/min.csv | geofhandler extract                   -o ${DIR}/to_append/min.csv
<${DIR}/to_append/min.csv geofhandler append -r ${DIR}/db && touch ${DIR}/appended/min.token
geofhandler export -vvvv -r ${DIR}/db -o ${DIR}/exported/min.csv -m ${DIR}/missing/min.csv ${DIR}/extracted/min.csv

# TODO: Vérif ce qu'il y a dans exported, missing et dans le repo
grep '15/12/2020' ${DIR}/missing/min.csv
grep '13/12/2020' ${DIR}/exported/min.csv
