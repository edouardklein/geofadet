#!/bin/bash
# convert used to crash when dealing with a <detailTrafic> where a number
# appears twice
# This test checks that this is not the case anymore
set -e
set -u
set -x
set -o pipefail

DIR=/tmp/geofadet-test

rm -rf ${DIR}
mkdir -p ${DIR}/union
mkdir -p ${DIR}/converted
mkdir -p ${DIR}/extracted

cp tests/no_timespan.xml ${DIR}/union/
<${DIR}/union/no_timespan.xml geofhandler convert -vvvv\
 >${DIR}/converted/no_timespan.csv 2>${DIR}/no_timespan0.log


<${DIR}/converted/no_timespan.csv geofhandler extract -vvvv\
 >${DIR}/extracted/no_timespan.csv 2>${DIR}/no_timespan1.log

# Check an error is reported
[ ! "`grep 'INFO : Partial data' ${DIR}/no_timespan1.log`" ]
grep 'WARNING : Timespan None for com' ${DIR}/no_timespan0.log
