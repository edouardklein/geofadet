all: install doc lint test

install:
	pip3 install -r requirements.txt
	pip3 install -U .


check_uninstall:  # https://stackoverflow.com/questions/47837071/making-make-clean-ask-for-confirmation
	@echo -n "This will delete EVERYTHING from geofadet, including the DATA and the CONF ! Proceed ? [y/N] " && read ans && [ $${ans:-N} == y ]

uninstall: check_uninstall
	-sudo pip3 uninstall geofadet
	-sudo pip3 uninstall gitkv
	-sudo pip3 uninstall daemux
	-sudo pip3 uninstall pmjqtools
	-sudo rm /usr/local/bin/pmjq
	-sudo rm /usr/local/bin/websocketd
	-sudo rm -r /usr/share/doc/geofadet
	-sudo rm -r /usr/share/geofadet
	-sudo rm -r /var/spool/geofadet
	-sudo rm -r /var/lib/geofadet
	-sudo rm -r /srv/geofadet
	-sudo rm -r /etc/geofadet
	#-guix package -r geofadet


clean: test_clean
	cd docs; make clean

doc:
	pmjq_viz --exec tools/transitions.py "T('ROOT', 'ASSETS', 'GITKV')" | dot -Tsvg -o docs/img/transitions.svg
	make -C docs html

test_clean:
	coverage erase
	-rm tests/test_tmp.gpickle  # Created in geofadet/readwrite.py doctests
	-rm tests/test_tmp.pdf      # Created in geofadet/readwrite.py doctests

test:
# Unit tests
	coverage run geofadet/__init__.py
	tests/double.sh
	tests/missing_number.sh
	tests/missing_target.sh
	tests/empty_corr_whitespaces.sh
	tests/wrong_indiv_quant.sh
	tests/too_many_cellid.sh
	tests/no_timespan.sh
# Integration tests
	tests/test.sh
	coverage report | grep 'geofadet/'

lint:
	flake8 $$(find . -type f -name '*.py' -not -path "./docs/*" -not -path "./tools/*" -not -path "./pmjq_geofadet_ui/*")

fixme:
	find . -type f | xargs grep --color -H -n -i fixme

todo:
	find . -type f | xargs grep --color -H -n -i todo

live:
	find . -type f -name '*.py' | entr make test

.PHONY: clean test lint fixme todo live doc check_uninstall uninstall
