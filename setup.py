from setuptools import setup, find_packages

__version__ = '0.5.2'

entry_points = {
    'console_scripts': [
        'geofparser = geofadet.__init__:geofparse',
        'geofhandler = geofadet.__init__:geofhandle',
        'geofconf = geofadet.__init__:geofconf'
    ],
}

setup(
    name='geofadet',
    version=__version__,
    description='Extract useful data from FADET files',
    license='AGPL',
    classifiers=[
        'Topic :: Scientific/Engineering :: Information Analysis',
    ],
    keywords='FIXME',
    packages=find_packages(exclude=['docs', 'tests*']),
    include_package_data=True,
    author='Edouard Klein',
    entry_points=entry_points,
    author_email='edouardklein -at- gmail.com'
)
