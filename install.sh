#!/usr/bin/env bash
set -euxo pipefail

# Site-specific configuration
DOMAIN=127.0.0.1  # Public domain name users will connect to
PORT=8080

BINARY_TAR_GZ=/tmp/geofadet.tar.gz
DOC_TAR_GZ=/tmp/geofadet-doc.tar.gz


# Sensible defaults
DOC_DIR=/usr/share/doc/geofadet
STATIC_ASSETS=/usr/share/geofadet
PMJQ_ROOT=/var/spool/geofadet
LOCALIZATION_DB=/var/lib/geofadet/localization_db
WEB_ROOT=/srv/geofadet/www
CONF_DIR=/etc/geofadet
USER=geofadet

# Installation
cd /
tar xvzf ${BINARY_TAR_GZ}
tar xvzf ${DOC_TAR_GZ}
echo 'export PATH=$PATH:/opt/gnu/bin/' >> ~root/.profile
source ~root/.profile
useradd ${USER}
git config --global user.email ${USER}@${DOMAIN}
git config --global user.name ${USER}
geofconf --documentation=${DOC_DIR} --static-assets=${STATIC_ASSETS} --pmjq-root=${PMJQ_ROOT} --gitkv=${LOCALIZATION_DB} --web-dir=${WEB_ROOT} --port=${PORT} --dl-url='http://'${DOMAIN}:${PORT}'/dl_zone/' --output=${CONF_DIR} --user=${USER}
