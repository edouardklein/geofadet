.. include:: links.rst

.. _ref_mt20_retro:
.. _inter-union:

**************************************
Retro-engineering the MT20 XML schema
**************************************

Practice
=========

Because an incredibly high proportion of files we get are fucked up one way or the
other, we have to very thouroughly check that the tags we expect are there, and that
there are no tags we do not expect.


To illustrate, let us consider the following XML file:

.. code-block:: xml

   <parent>
                <child1>Content</child1>
                <child1>Repeated tags are merged together</child1>
                <child1>We only care about what may appear, and a vague idea of how often</child1>
                <child1>
                <grandchild>But we don't care about the content yet</grandchild>
                </child1>
                <child2>Another tag</child2>
   </parent>


By running the commands ``geofparser union --color -o toto.dot toto.xml && dot -Tpdf toto.dot > toto.pdf``, one gets:

.. image:: img/example_tree.svg


.. warning:: FIXME: auto tests

   These examples should be run each time we build the doc, as a mean to test the code

One can see here that a ``parent`` tag always (the blue color shows so) has a ``child1`` and a ``child2`` tag inside of it. A ``child1`` however very often (green=often, red=rarely) does not contain a ``grandchild``, but may do so.

The numbers on the edge let us now how often exactly a child is present.

The ``geofhandler`` program (:py:mod:`geofadet.handler`) is used with two files, ``UNION.gpickle`` and ``INTER.gpickle``, to check whether a FADET respectively contains:

* only tags we have come to expect (its structure must be a subtree of UNION)
* all the tags we have come to expect (INTER must be a subtree of its structure)

To this end, we took a bunch of knwown somewhat good MT20 and MT23 FADETs, and
extracted their internal structure into trees. We then merged all those trees together into the UNION and INTER files, which we then put under version control.

We can not put the original FADET files under version control, however, as they contain confidential information :/

* ``geofparser union --color --gpickleout -o tools/refs/UNION.gpickle FADETS/\*.xml``
* ``geofparser inter --gpickleout -o tools/refs/INTER.gpickle FADETS/\*.xml``

Once exported to dot:

* ``geofparser union --color --gpicklein -o UNION.dot tools/refs/UNION.gpickle``
* ``geofparser union --gpicklein -o INTER.dot tools/refs/INTER.gpickle``

the resulting union tree is given below.

.. image:: img/union.svg

And the resulting intersection tree is:

.. image:: img/inter.svg

This was our first step in retroengineering the MT20 XML format. We can see a lack of care in its definition:

* The tags are written in french, with typos (e.g. messageEnvoy\ **er** instead of MessageEnvoy\ **é**)
* No attempt whatsoever at a hierarchy that makes sense:

    * Multiple times, a tag will have one and only one child, making one of those extraneous, e.g.

        * ``messageEnvoyer`` contains one and only one ``messageReponse``
        * ``parametresComplementairesReponse`` contains one and only one ``identifiantReponse``
        * ``reponseprestation`` contains one and only one ``reponsetrafic``
        * ``numeroTelephone`` contains one and only one ``TelephoneInternational``
        * ``localisation`` contains one and only one ``cellule``

    * ``adresse2`` is the child of ``adresse``
    * ``adresse3`` is the sibling of ``adresse2``
    * There is no ``adresse1``
    * There are six tags containing the word ``reponse``,

        * all of which are children of ``messagereponse``,
        * itself the only child of ``messageenvoyer``.
        * ``messageEnvoyer`` and ``messageReponse`` should be merged into a single tag (``reponse``, or better ``payload`` (in opposition with ``header``), ``data`` (in opposition with ``metadata``), etc.)
        * all tag names with ``reponse`` in them should have the ``reponse`` removed, the fact that they are children of a ``response`` tag should be enough context to understand what is meant.
        * The XML norm itself acknowledges this with the `XPATH syntax`_. In summary, instead of having e.g. ``/message/messageenvoyer/messagereponse/parametrescomplementairesreponse/identifiantreponse/``, we could have had ``/mt20/data/id``. XML sucks, but this particular schema manages to suck even more.



  
Theory
======

Validating XML sucks
----------------------

There is much to say about the validation of XML files. There exists dedicated technologies, but all are doomed. The gist of the argument is that the language in which to express the validation rules must be:

* Either not Turing-Complete, which means it is not expressive enough and it sucks
* Or accidentally Turing complete (see `Accidentally Turing 1`_, `Accidentally Turing 2`_) which means its syntax is cthuluesque and it sucks
* Or purposely Turing-Complete in which case the syntax is still awful and it still sucks

All these problems have been solved decades ago: S-Expression have the more representative power than XML, a simpler and cleaner syntax, and it is trivial to validate a S-exp tree: an elegant method is by executing the tree itself in a LISP interpreter.

.. epigraph::
    We can easily write an S-expression which encodes type and value constraint information about another S-expression. Those constraints will be applied over data which is already richly annotated with type information; we can validate that the third element of some list is an integer, and if it passes that test, we can then validate that it satisfies some property that integers can have.

-- `C2 - Poor Copy`_

The C2 wiki has a good outlook of why S-exps are superior to XML in many ways (see `C2 - Poor Copy`_, `C2 - XML Sucks`_, `C2 - Lisp VS XML`_).

Rivest (the R in RSA) proposed a standard of S-expressions (see `S-Expressions RFC`_).



