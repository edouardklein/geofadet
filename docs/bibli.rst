.. include:: links.rst

============
Bibliography
============

* Turing completeness:
    
    * `Accidentally Turing 1`_ 
    * `Accidentally Turing 2`_ 

* XML
    * `C2 - Poor Copy`_

    * `C2 - XML Sucks`_

    * `C2 - Lisp VS XML`_

    * `S-Expressions RFC`_

* Other

    * `FHS`_

* Software

    * `QGIS`_

    * `GUIX`_

    * `HERD`_
    
