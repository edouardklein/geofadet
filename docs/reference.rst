***********
Reference
***********

.. _visualisation:

Structure visualisation
=======================

The :py:mod:`geofadet.parser` module provides various visualisation tools to see or compare FADETs' structure.

To see the whole parser API, use::

    geofparser --help


For visualisation, the easy way is to use the **dot** command from `Graphviz <http://graphviz.org/>`_ to obtain a PNG file.

Get the structure of a single file
----------------------------------

::

    geofparser union my_FADET.xml | dot -Tpng -Gdpi=300 -o output.png


Get the union of many structures and color it by frequencies
------------------------------------------------------------

::

    geofparser union --color *.xml | dot -Tpng -Gdpi=300 -o output.png



Get the intersection of structures
----------------------------------

::

    geofparser inter my_FADET.xml my_other_FADET.xml | dot -Tpng -Gdpi=300 -o output.png


See the difference between two structures
-----------------------------------------

::

    geofparser compare -f from_FADET.xml -t to_FADET.xml | dot -Tpng -Gdpi=300 -o output.png


.. _struct-extract:

Structure extraction
====================

The extracted structures are part of GeoFADET's internal structure. To find more details, see :ref:`the topic on this subject <inter-union>`.

Union
-----

::

    geofparser union *.xml --gpickleout -o UNION.gpickle

Intersection
------------

::

    geofparser inter *.xml --gpickleout -o INTER.gpickle


.. automodule:: geofadet.graph_utils
   :members:

.. automodule:: geofadet.handler
   :members:

.. automodule:: geofadet.parser
   :members:

.. automodule:: geofadet.conf
   :members:

.. automodule:: geofadet.readwrite
   :members:
