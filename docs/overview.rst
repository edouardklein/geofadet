.. include:: links.rst

********
Overview
********

GeoFADET
========

GeoFADET is a software and toolset made to analyze and process **FADETs**, telecommunication files containing phone communications metadata given by telecom providers and then modified and unified by the PNIJ.

The FADETs that GeoFADET targets are **MT20 FADETs** (XML files), which *supposedly* contain **localisation** data of the antennas the target phone used.


Why?
====

Even though the XML format is *mostly* unified, we found out that the contained data was highly **variable**, often **incomplete**, and sometimes (which is already way too much) completely **wrong or inconsistent** (see  :ref:`the detailed explanation subject <inter-union>`).


Use cases
=========

GeoFADET contains various tools that can be used to manipulate FADETs in many ways. For example, you can:

* **Extract localization data** in a readable format (CSV), to be able to reuse it or give it to a map visualisation software (like `QGIS`_)
* **Find incorrect data** and extract it, to be able to ask telecom providers to fix it
* Get a **graphical overview** of the FADETs' structure, compare some/all of them to see the differences between them
* Fill a **database** with previously known localizations, which can be useful in some cases to complete missing data on telecom events that would use the same antennas during the same time period.
