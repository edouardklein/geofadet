.. _Accidentally Turing 1: https://www.gwern.net/Turing-complete 
                          
.. _Accidentally Turing 2: http://beza1e1.tuxen.de/articles/accidentally_turing_complete.html

.. _C2 - Poor Copy: http://wiki.c2.com/?XmlIsaPoorCopyOfEssExpressions

.. _C2 - XML Sucks: http://wiki.c2.com/?XmlSucks

.. _C2 - Lisp VS XML: http://wiki.c2.com/?LispVsXml

.. _S-Expressions RFC: http://people.csail.mit.edu/rivest/Sexp.txt


.. _XPATH syntax: https://www.w3schools.com/xml/xpath_syntax.asp

.. _QGIS: https://www.qgis.org

.. _GUIX: https://www.gnu.org/software/guix/

.. _FHS: http://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html

.. _HERD: https://www.gnu.org/software/shepherd/
