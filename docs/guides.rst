.. include:: links.rst

******
Guides
******

Usage
======

.. warning::
   FIXME: Put screen captures here

Using a web browser, the user simply drops one MT20 file, and get 4 files in return:

* A .csv file with all localized communication events
* A .csv file with all communication events that lack a localization
* A .csv file with both localized and non localized communication events
* A log file with all communication events that were thrown out because they were inconsistent


Installation
============

Guix installation
-----------------

GeoFADET is packaged using GNU Guix (`GUIX`_), a functionnal package manager which aims at reproducible builds.

Given that geofadet is made up of many small, interconnected parts written in different languages and is expected to run on many different systems, we needed such a powerful tool as guix.

Users of the Guix package Manager can install geofadet with::

    guix package -i geofadet


On a system where Guix is not installed, one can simply untar the archive::

    tar xvzf geofadet.tar.gz


See :ref:`Architecture <ref_architecture>`, :ref:`Packaging <ref_packaging>` and the following section (:ref:`Configuration <ref_configuration>`) to understand what geofadet is made of and where those parts are installed.

.. _ref_configuration:

Configuration
-------------

Being a *functional* package manager, Guix can not impact the operating system it is run on in any other way than by carefully writing and reading to and from the *store*, a directory usually located in `/gnu/store/`, and to and from the user profile (usually `~/.guix-profile`).

Any other operation falls outside of Guix's umberella, as it would be non reversible, non composable, etc.

Guix can be used to manage the whole operating system. The resulting Linux Distribution, GuixSD, can do composable, roll-backable modifications to the OS configuration. The whole system configuration is stored in a file written in a declarative domain-specific language that allow one to fully specify all aspects of the OS configuration.

Because `geofadet` is expected to run on any linux system (and hopefully any POSIX-compliant system), this facility can not be used.

Instead, after the installation, we let the user decide what the impact of `geofadet` on the target system will be. Conversely, the public hostname of the machine (a piece of configuration unavailable to Guix at install-time) is stored in `geofadet` 's configuration.

This non-functional but necessary step is done through the `geofconf` utility.

The user is expected to provide this utility with the information detailed below. Defaults confirming to the Filesystem Hierarchy Standard (`FHS`_) are provided.

Documentation
    The doc of geofadet and its supporting projects (GitKV, Daemux, PMJQ) should be located in `/usr/share/doc`.

Static assets
    The static assets of Geofadet are the `UNION.gpickle`, `INTER.gpickle` files (see :ref:`the MT20 retro engineering section <ref_MT20_retro>`) and the transitions file `transitions.py` (see :ref:`PMJQ <ref_pmjq>`).

    We think they should be stored in `/usr/share/geofadet`.

Endpoints
    PMJQ needs a subdirectory in which to store the data being processed. The obvious choice is `/var/spool/geofadet/`

Antenna Geolocation Database
    GitKV needs a place to store the timestampped localisation of the antennas. We suggest `/var/lib/geofadet/localization_db`.

Web zone
    The websocket and web server must serve HTML, JS and images. These files should go in `/srv/geofadet/www/`. This is also where will be created the files the user will downloaded after their request is processed (specifically in `/srv/geofadet/www/dl_zone/`).

URL of the DL folder
    The folder in which will be put the files the user expects must be accessible from outside. `geofconf` needs to know the url of this folder.
    
Configuration dir
    `geofconf` stores all this information in files in `/etc/geofadet/` unless instructed otherwise.

User
    `geofadet` will run either as root or as a user. Running as root is OK if and only if Geofadet is the only service that will run on the computer, and there is no possible problem with having an adversary being root on one machine in your network. It is therefore rarely OK.

    All of `geofadet` 's files and directories will be `chown` -ed to this user by `geofconf`.

A call to `geofconf` with all these defaults will look like::

  geofconf --documentation=/usr/share/doc/geofadet --static-assets=/usr/share/geofadet/ --pmjq-root=/var/spool/geofadet/ --gitkv=/var/lib/geofadet/localization_db --web-dir=/srv/geofadet/www/ --port=8080 --dl-url='http://127.0.0.1:8080/dl_zone' --output=/etc/geofadet/ --user=geofadet


Start, stop, status
-------------------

Once run, `geofconf` will generate multiple `*.scm` files, which describe GNU shepherd (`HERD`_) services.

To run `geofadet`, one has to first start `shepherd` using those configuration files::

    shepherd --config=/etc/geofadet/geofadet.scm

Then, one can simply connect to the `shepherd` daemon using the `herd` command::

    herd start geofadet
    herd status geofadet
    herd stop geofadet

Each individual PMJQ transition (see :ref:`PMJQ <ref_pmjq>`) also has its own herd services, which will be launched when starting `geofadet`. Those services can be inspected individually if need be::

    herd status inter

.. _ref_packaging:

Packaging
----------

`geofadet` is made of multiple small, articulated parts (see the :ref:`architecture <ref_architecture>`).

A `GUIX`_ package of the same name has been written, which include all parts of geofadet and their dependencies.

Guix can be used to reproducibly create a tarball with geofadet and all its dependencies.

It is possible to make it so this tarball, when unpacked on the target system, add links from the *store* (the only part of the filesystem that Guix is allowed to modify) to standard directories (e.g. `/usr/share/doc/geofadet`): https://www.gnu.org/software/guix/manual/en/guix.html#Invoking-guix-pack

.. warning::
    FIXME: Do it!

We added in the Makefile the `pack` target which will generate such a tarball, with the following options to `guix pack`::

    -S /usr/share/doc=share/doc # The HTML doc of geofadet and associated subprojects

    -S /usr/share/geofadet=share/geofadet # The static assets of geofadet

    -S /srv/geofadet/www=srv/geofadet/www # The web server root

The other standard directories can only be created at configuration time by `geofconf` (see :ref:`configuration <ref_configuration>`).

.. _ref_architecture:

Architecture
============

`geofadet` is made up of:

* the `geofadet` Python module, which includes the `geofparser`, `geofhandler` and `geofconf` binaries,
* the HTML/CLJS interface (`pmjq_geofadet_ui`),
* the `websocketd` server which serves the web interface files and handles its communication with
* the Python backend, `pmjq_sff` which will read and write files to
* the `pmjq` flow;
* GNU shepherd to manage all of this,
* and GNU Guix to install, update and deploy.


.. warning::
    FIXME: Draw this
    

.. _ref_pmjq:

Processing chain and commands
=============================

Although GeoFADET can be used as a toolset, it is designed to be run as a processing chain through the use of PMJQ.

Here is a visualisation of the processing chain described by tools/transitions.py:


.. _toolchain:
.. image:: img/transitions.svg


This chain works in the following way:

- The input FADETs have their structure analysed and compared to the reference structures

- The files are then converted to a CSV format, while erroneous data is filtered and described in a log file

- The localisation data will be extracted, and also added to the database

- The extracted CSV will be used as a basis and filled with data coming from the database, to gather the relevant localisation information

- The result is split into three files, one with localisation data, one with missing data, and one with the merged content of those two

.. warning::
    FIXME: Pas de geofparser ci dessous ?


This chain can be run by hand using the following commands::

    geofhandler supercheck ~/geofadet/tools/refs/INTER.gpickle my_FADET.xml
    geofhandler subcheck ~/geofadet/tools/refs/UNION.gpickle my_FADET.xml
    geofhandler convert my_FADET.xml > converted.csv 2> convert.log
    geofhandler extract converted.csv > extracted.csv
    geofhandler append -r my_git_database extracted.csv
    geofhandler export -r my_git_database -o exported.csv -m missing.csv converted.csv

Transitions.py
--------------

The transition.py file is used to describe the :ref:`processing chain <toolchain>` that will be started by PMJQ.

Such a file contains at least a variable containing the transitions (as a list), but can also include more code
(e.g. the default file for GeoFADET makes an import and a variable declaration before creating the actual transitions).

To use such a transition file with PMJQ, the filepath should be given as the ``--exec`` argument, and the variable name (e.g. "T") as the ``<eval>`` one.


To get more details about the contents of the transition file, please refer to PMJQ's documentation.

Common errors/logs
==================

inter
-----

An error during the execution of the ``inter`` command (**"The graph doesn't follow the given schema !"**) means that the FADET's structure lacks some nodes.
This requires manual verification as to determine whether this is an error in the file, or in the reference used (INTER.gpickle).

union
-----

The same error during the ``union`` command, as with ``inter``, shows a discrepancy between the reference structure and the file's.
Once again, a manual verification is needed. If the FADET is valid, then the UNION.gpickle reference structure has to be changed to include the new structure.


.. _convert-errors:

convert
-------

``convert`` will reject incoherent or erroneous files, and notify the user with warnings.
Common cases:

    * **"Too many/few participating parties"**: A communication is expected to have between 1 and 3 participating parties. The communication will be rejected otherwise.
    * **"One party has no number"**: Since parties are identified by a phone number, it is nonsensical to have parties without any.
    * **"[...] uses the target 0 or more than 1 time"**: It is nonsensical to have a party communicate with themselves, as well as for the targeted party to be absent in a communication.
    * **"Duplicate number(s)"**: It is nonsensical to have the same party more than once (and so, with more than one role) in a single communication.
    * **"Phone number [...] is localized, but should not !"**: A party who isn't the target shouldn't be localised by the phone operator.
